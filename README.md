# [3 Simple Ways to Avoid Stressed Out Developers](https://appdev-coe.gitlab.io/3-simple-ways-to-avoid-stressed-out-developers)

 - [Lightning talk session for 3 Simple Ways to Avoid Stressed Out Developers](https://appdev-coe.gitlab.io/3-simple-ways-to-avoid-stressed-out-developers/lightning-talk.html)

 - [Full session for 3 Simple Ways to Avoid Stressed Out Developers](https://appdev-coe.gitlab.io/3-simple-ways-to-avoid-stressed-out-developers/full-talk.html)


## Abstract:
The life of a developer is mind-numbing, soul crushing, and yet so intriguing that we don’t walk away. Why is that? Why do you enjoy the stress of working in application development? Let’s take a look at three simple ways to remove some of the devastation that stressful developers leave behind. In this session the attendee leaves with simple tips that are applicable to their daily lives and can be shared with their teams. Join us for an hour of power that guides you on the path to rest, relaxation, and unadulterated coding your hearts out.

 - Start too early

 - Going Live missing

 - Communication


[![Cover Slide](https://gitlab.com/appdev-coe/3-simple-ways-to-avoid-stressed-out-developers/raw/master/cover.png)](https://appdev-coe.gitlab.io/3-simple-ways-to-avoid-stressed-out-developers)
